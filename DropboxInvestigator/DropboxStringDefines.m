//
//  stringDefines.m
//  OPSDBDownloadManager
//
//  Created by David Eccles on 04/07/2016.
//  Copyright © 2016 Open Planet. All rights reserved.
//

#import "DropboxStringDefines.h"

@implementation DropboxStringDefines

NSString * const kDB_FolderKey          = @"folderKey";
NSString * const kDB_FileKey            = @"fileKey";

NSString * const kDB_FolderValue        = @"folderVale";
NSString * const kDB_FileValue          = @"fileValue";

NSString * const kDB_pathKey            = @"pathKey";
NSString * const kDB_OnDeviceKey        = @"onDeviceKey";
NSString * const kDB_OnDevicePathKey    = @"onDevicePathKey";

NSString * const kDB_MediaKey           = @"mediaKey";
NSString * const kDB_RevKey             = @"revisionKey";
NSString * const kDB_IDKey              = @"IDKey";
NSString * const kDB_UpdateKey          = @"updateKey";

NSString * const kDB_FolderCellID       = @"FolderCell";
NSString * const kDB_NotLocalFileCellID = @"FileCell";
NSString * const kDB_LocalFileCellID    = @"LocalFileCell";

NSString * const kDB_FolderImage        = @"Folder";
NSString * const kDB_DownloadImage      = @"Download";

NSString * const kDB_SignInSegueID      = @"signIn";
NSString * const kDB_FolderSegueID      = @"FolderSegue";

NSString * const kDB_RootFolderKey      = @"Dropbox_Root_Folder";
NSString * const kDB_DefaultsIDKey      = @"userDefaultsAccountID";

NSString * const kDB_DBHomeFolderTitle  = @"Dropbox Folder";

NSString * const kDB_DBRootFolderPath   = @"";

NSString * const kDB_UpdateButtonText   = @"Update";
NSString * const kDB_ContentsFilePostfix= @"_CONTENTS";

NSString * const kDB_CellularBool       = @"useCellular";

NSString * const kDB_VersionedKey       = @"Versioned";
NSString * const kDB_UnVersionedKey     = @"UnVersioned";









@end
