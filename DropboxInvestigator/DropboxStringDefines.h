//
//  stringDefines.h
//  OPSDBDownloadManager
//
//  Created by David Eccles on 04/07/2016.
//  Copyright © 2016 Open Planet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DropboxStringDefines : NSObject

extern NSString * const kDB_FolderKey;
extern NSString * const kDB_FileKey;

extern NSString * const kDB_FolderValue;
extern NSString * const kDB_FileValue;

extern NSString * const kDB_pathKey;
extern NSString * const kDB_OnDeviceKey;
extern NSString * const kDB_OnDevicePathKey;

extern NSString * const kDB_MediaKey;
extern NSString * const kDB_RevKey;
extern NSString * const kDB_IDKey;
extern NSString * const kDB_UpdateKey;

extern NSString * const kDB_FolderCellID;
extern NSString * const kDB_NotLocalFileCellID;
extern NSString * const kDB_LocalFileCellID;
extern NSString * const kDB_FolderImage;
extern NSString * const kDB_DownloadImage;

extern NSString * const kDB_SignInSegueID;
extern NSString * const kDB_FolderSegueID;

extern NSString * const kDB_RootFolderKey;
extern NSString * const kDB_DefaultsIDKey;

extern NSString * const kDB_DBHomeFolderTitle;
extern NSString * const kDB_DBRootFolderPath;

extern NSString * const kDB_UpdateButtonText;
extern NSString * const kDB_ContentsFilePostfix;

extern NSString * const kDB_CellularBool;

extern NSString * const kDB_VersionedKey;
extern NSString * const kDB_UnVersionedKey;








@end
