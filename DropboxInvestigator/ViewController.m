//
//  Copyright © 2018, OPS.
//  http://www.openplanetsoftware.com
//
//  If you have received this software as part of a source code distribution
//  from OPS, subject to the limits of a supporting
//  contractual agreement, permission to use, copy, modify, this software
//  is hereby granted.
//
//  All use of this software in products not owned or commissioned by
//  OPS requires the written permission of OPS.
//

#import "ViewController.h"
#import <ObjectiveDropboxOfficial/ObjectiveDropboxOfficial.h>


@interface ViewController ()

@end

@implementation ViewController

/*
 Do files downloaded in our app also appear as downloaded in the Files app and the Dropbox app?
 
 If a file is downloaded in the files app / dropbox app can we accees it or do we need to download our own copy?
 When we do downlaod a file, where does it go and how 'stable' is the URL to that file - does s survive restarts? Can we use it for sending to Spotlight / transctipions etc
 
 We should be abe to make / delete folders.  Add / remove files.  Download files (edited)
 
 And see how things interact with the Files /Dropbox apps.
 */

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

}

- (void)viewDidAppear:(BOOL)animated
{
    [DBClientsManager authorizeFromController:[UIApplication sharedApplication]
                                   controller:self
                                      openURL:^(NSURL *url)
     {
         [[UIApplication sharedApplication] openURL:url];
     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
