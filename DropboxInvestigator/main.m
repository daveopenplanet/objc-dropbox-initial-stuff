//
//  Copyright © 2018, OPS.
//  http://www.openplanetsoftware.com
//
//  If you have received this software as part of a source code distribution
//  from OPS, subject to the limits of a supporting
//  contractual agreement, permission to use, copy, modify, this software
//  is hereby granted.
//
//  All use of this software in products not owned or commissioned by
//  OPS requires the written permission of OPS.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
