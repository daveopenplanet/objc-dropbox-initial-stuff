//
//  Copyright © 2018, OPS.
//  http://www.openplanetsoftware.com
//
//  If you have received this software as part of a source code distribution
//  from OPS, subject to the limits of a supporting
//  contractual agreement, permission to use, copy, modify, this software
//  is hereby granted.
//
//  All use of this software in products not owned or commissioned by
//  OPS requires the written permission of OPS.
//

#import "DbApiWrapper.h"
#import "DropboxStringDefines.h"
#import <ObjectiveDropboxOfficial/ObjectiveDropboxOfficial.h>

@interface DbApiWrapper ()
@end

@implementation DbApiWrapper

- (void)linkDBFrom:(UIViewController*)controller
{
    if (self.isSignedIn == NO)
    {
        [DBClientsManager authorizeFromController:[UIApplication sharedApplication]
                                       controller:controller
                                          openURL:^(NSURL *url)
         {
             [[UIApplication sharedApplication] openURL:url];
         }];

    }
    else
    {
        NSLog(@"User is already authorized!");
    }
}

- (void)searchDropboxForFileName:(NSString*)fileName inFolder:(NSString*)folderPath
{
    //- (DBRpcTask *)search:(NSString *)path query:(NSString *)query
    //path = folder query = filname
    
    DBUserClient* client = [DBClientsManager authorizedClient];
    DBRpcTask* searchTask = [client.filesRoutes search:folderPath query:fileName];
    [searchTask setResponseBlock:^(DBFILESSearchResult* searchResult, DBFILESSearchError* searchRouteError, DBRequestError * _Nullable networkError)
     {
         if (searchResult.matches.count > 0)
         {
             NSLog(@"🤩SEARCH RESULT == %@", searchResult);
             for (DBFILESSearchMatch* match in searchResult.matches)
             {
                 if([match.metadata.name isEqualToString:fileName])
                 {
                     NSLog(@"🤯EXACT MATCH🤯");
                     break;
                 }
                 else
                 {
                     NSLog(@"🙈NOT EXACT MATCH🙈");
                 }
             }
         }
         else
         {
             NSLog(@"😡SEARCH ERROR == %@", searchRouteError);
         }
     }];
}


- (BOOL)isSignedIn
{
    return [DBClientsManager authorizedClient] != nil;
}

- (void)uploadData:(NSData*)data ToLocation:(NSString*)locationString
{
    NSString* parentLocation = [locationString stringByDeletingLastPathComponent];
    [self listFilesAtPath:parentLocation withCompletion:^(BOOL listSuccess)
    {
        if(listSuccess)
        {
            //got some files back from parentFolder, do we have THE file?
            BOOL fileExists = NO;
            for(NSString* fileName in self.currentPathDictionary.allKeys)
            {
                if ([fileName isEqualToString:locationString.lastPathComponent])
                {
                    //file exists in dropbox already..
                    fileExists = YES;
                    break;
                }
            }
            if (fileExists == NO)
            {
                //folder exists but file doesn't, can upload file
                DBUserClient* client = [DBClientsManager authorizedClient];
                DBUploadTask* task = [client.filesRoutes uploadData:locationString inputData:data];
                [task setResponseBlock:^(DBFILESFileMetadata *result, DBFILESUploadError *routeError, DBRequestError *networkError)
                {
                    if (result)
                    {
                        NSLog(@"%@\n", result);
                    }
                    else
                    {
                        NSLog(@"🤯%@\n%@\n", routeError, networkError);
                    };
                }];
            }
        }
        else
        {
            //parent folder doesn't exist
            DBUserClient* client = [DBClientsManager authorizedClient];
            DBRpcTask* folderTask = [client.filesRoutes createFolder:parentLocation];
            [folderTask setResponseBlock:^(DBFILESFolderMetadata *result, DBFILESCreateFolderError *routeError, DBRequestError *networkError)
            {
                 if (result)
                 {
                     NSLog(@"%@\n", result);
                     //folder created, now can upload file..
                     DBUploadTask* task = [client.filesRoutes uploadData:locationString inputData:data];
                     [task setResponseBlock:^(DBFILESFileMetadata *result, DBFILESUploadError *routeError, DBRequestError *networkError)
                      {
                          if (result)
                          {
                              NSLog(@"%@\n", result);
                          }
                          else
                          {
                              NSLog(@"🤯%@\n%@\n", routeError, networkError);
                          };
                      }];
                 }
                 else
                 {
                     NSLog(@"%@\n%@\n", routeError, networkError);
                 }
             }];
        }
    }];
}

- (void)listFilesAtPath:(NSString*)path withCompletion:(void (^)(BOOL listSuccess))completion
{
    if (self.isSignedIn == YES)
    {
        DBUserClient* client = [DBClientsManager authorizedClient];
        DBRpcTask* listTask = [client.filesRoutes listFolder:path];
        [listTask setResponseBlock:^(DBFILESListFolderResult *response, DBFILESListFolderError *routeError, DBRequestError *networkError)
        {
            if (response != nil)
            {
                self.currentPathDictionary = [NSMutableDictionary dictionary];
                NSArray<DBFILESMetadata *>* entries = response.entries;
                for (DBFILESMetadata* entry in entries)
                {
                    NSMutableDictionary* entryDictionary = [NSMutableDictionary dictionary];
                    NSNumber* isFolder = [NSNumber numberWithBool:NO];
                    if ([entry isKindOfClass:[DBFILESFolderMetadata class]])
                    {
                        isFolder = [NSNumber numberWithBool:YES];
                    }
                    else if([entry isKindOfClass:[DBFILESFileMetadata class]])
                    {
                        entryDictionary[kDB_RevKey] = [(DBFILESFileMetadata*)entry rev];
                        entryDictionary[kDB_IDKey] = [(DBFILESFileMetadata*)entry id_];
                    }
                    [entryDictionary setObject:entry.pathDisplay forKey:kDB_pathKey];
                    [entryDictionary setObject:isFolder forKey:kDB_FolderKey];
                    [self.currentPathDictionary setObject:entryDictionary forKey:entry.name];
                }
                completion(YES);
            }
            else
            {
                completion(NO);
            }
        }];
    }
}

- (void)downloadFrom:(NSString*)path to:(NSString*)destination completion:(void (^)(BOOL success, NSString* error))completion
{
    if (self.isSignedIn)
    {
        DBUserClient* client = [DBClientsManager authorizedClient];
        
        NSArray* cacheUrls = [[NSFileManager defaultManager] URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask];
        NSURL* destinationDir = [cacheUrls[0] URLByAppendingPathComponent:kDB_DBRootFolderPath];
        destinationDir = [destinationDir URLByAppendingPathComponent:destination];
        //😳 this destination needs to match the one we have just created in folderManager...
        
        NSLog(@"🔥wrapper destination %@🔥",destinationDir);
        
        DBDownloadUrlTask* downloadTask = [client.filesRoutes downloadUrl:path overwrite:YES destination:destinationDir];
        [downloadTask setResponseBlock:^(DBFILESFileMetadata *result, DBFILESDownloadError *routeError, DBRequestError *networkError, NSURL *destination)
         {
             if (result)
             {
                 completion(YES, nil);
             }
             else
             {
                 NSLog(@"👺route error = %@ -- network error = %@", routeError, networkError);
                 completion(NO, @"downlaod error!! 😬");
             }
         }];
    }
}


@end
