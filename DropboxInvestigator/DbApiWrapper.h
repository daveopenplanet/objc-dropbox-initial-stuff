//
//  Copyright © 2018, OPS.
//  http://www.openplanetsoftware.com
//
//  If you have received this software as part of a source code distribution
//  from OPS, subject to the limits of a supporting
//  contractual agreement, permission to use, copy, modify, this software
//  is hereby granted.
//
//  All use of this software in products not owned or commissioned by
//  OPS requires the written permission of OPS.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DbApiWrapper : NSObject

@property (nonatomic) NSMutableDictionary* currentPathDictionary;

- (BOOL)isSignedIn;

- (void)linkDBFrom:(UIViewController*)controller;

- (void)uploadData:(NSData*)data ToLocation:(NSString*)locationString;

- (void)listFilesAtPath:(NSString*)path withCompletion:(void (^)(BOOL listSuccess))completion;

- (void)downloadFrom:(NSString*)path to:(NSString*)destination completion:(void (^)(BOOL success, NSString* error))completion;

- (void)searchDropboxForFileName:(NSString*)fileName inFolder:(NSString*)folderPath;


@end
